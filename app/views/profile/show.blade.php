@extends('layouts.default')

@section('content')

<header class="intro-header" style="background-image: url('/img/about-bg.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="page-heading">
                        <h1>{{ $user->name }}</h1>
                        <hr class="small">
                        <span class="subheading">{{ '@' . $user->username }}</span>
                    </div>
                </div>
            </div>
        </div>
</header>

<div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                
				@foreach ($articles as $article)

                <div class="post-preview">
                    <a href="/{{ $article->slug }}">
                        <h2 class="post-title">
                           {{ $article->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ $article->excerpt }}
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="/profile/{{ $article->author->username }}">{{ $article->author->name }}</a> on {{ $article->created_at }}</p>
                </div>
                <hr>

                @endforeach

                <!-- Pager -->
                {{ $articles->links() }}

            </div>
        </div>
</div>

@stop