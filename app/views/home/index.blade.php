@extends('layouts.default')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

            	@foreach ($articles as $article)

                <div class="post-preview">
                    <a href="/{{ $article->slug }}">
                        <h2 class="post-title">
                           {{ $article->title }}
                        </h2>
                        <h3 class="post-subtitle">
                            {{ $article->excerpt }}
                        </h3>
                    </a>
                    <p class="post-meta">Posted by <a href="/profile/{{ $article->author->username }}">{{ $article->author->name }}</a> on {{ $article->created_at }}</p>
                </div>
                <hr>

                @endforeach

                <!-- Pager -->
                {{ $articles->links() }}
            
            </div>
        </div>
    </div>

@stop