@extends('layouts.default')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p>Want to get in touch with me? Fill out the form below to send me a message and I will try to get back to you within 24 hours!</p>

                {{ Form::open(['route' => 'save']) }}

                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Title</label>
                            <input type="text" class="form-control" placeholder="Title" id="title" name="title" required="" data-validation-required-message="Please enter your title." aria-invalid="false">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>excerpt</label>
                            <input type="text" class="form-control" placeholder="excerpt" id="excerpt" name="excerpt" required="" data-validation-required-message="Please enter your excerpt." aria-invalid="false">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label for="status" class="">Status</label>
                            <select name="status_id" id="status">
                                @foreach($statuses as $status)
                                <option value="{{ $status->id }}">{{ $status->status }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                               <label for="tags" class="inline">Tags</label>
                               <input class="text input" type="text" name="tags" id="tags" placeholder="Tags" value="@if( isset($input['tags']) ){{ $input['tags'] }}@endif">
      
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Content</label>
                            <textarea rows="5" class="form-control" placeholder="Content" id="content" name="content" required="" data-validation-required-message="Please enter a content." aria-invalid="false"></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                    </div>
                
                {{ Form::close() }}

            </div>
        </div>
    </div>

@stop