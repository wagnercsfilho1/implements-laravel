<?php namespace Impl\Repo;

use Article; // Eloquent article
use Tag;
use User;
use Status;
use Impl\Repo\Tag\EloquentTag;
use Impl\Repo\Article\EloquentArticle;
use Impl\Repo\User\EloquentUser;
use Illuminate\Support\ServiceProvider;
use Impl\Service\Cache\LaravelCache;
use Impl\Repo\Status\EloquentStatus;

class RepoServiceProvider extends ServiceProvider{

	public function register()
	{
		$this->app->bind('Impl\Repo\Tag\TagInterface', function($app){
			return new EloquentTag( new Tag);
		});

		$this->app->bind('Impl\Repo\Article\ArticleInterface', function($app)
        {
            $article =  new EloquentArticle(
                new Article,
                $this->app->make('Impl\Repo\Tag\TagInterface'),
                new LaravelCache($app['cache'], 'articles', 1)
            );

            return $article;
        });

        $this->app->bind('Impl\Repo\User\UserInterface', function($app){
            return new EloquentUser( new User );
        });

        $this->app->bind('Impl\Repo\Status\StatusInterface', function($app)
        {
            return new EloquentStatus(
                new Status
            );
        });

	}

}