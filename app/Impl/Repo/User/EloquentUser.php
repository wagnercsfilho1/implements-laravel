<?php namespace Impl\Repo\User;

use Illuminate\Database\Eloquent\Model;

class EloquentUser implements UserInterface {

	public function __construct(Model $user)
	{
		$this->user = $user;
	}

	public function byUsername($username)
	{
		return $this->user->with('articles')->where('username', $username)->first();
	}

}