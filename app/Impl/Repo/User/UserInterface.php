<?php namespace Impl\Repo\User;

interface UserInterface {

	/**
     * Retrieve user by username
     * regardless of status
     *
     * @param  string $username User Username
     * @return stdObject object of article information
     */
	public function byUsername($username);

}