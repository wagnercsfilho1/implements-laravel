<?php

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

Route::get('/', 'HomeController@index');
Route::get('/create', ['as' => 'create', 'uses' => 'HomeController@create']);
Route::post('/save', ['as' => 'save', 'uses' => 'HomeController@save']);


Route::get('/profile/{username}', 'ProfileController@show');

Route::get('/{slug}', 'HomeController@show');

Route::group(array('prefix' => 'admin'), function()
{
	Route::resource('article', 'ArticleController');
});

//Other Exceptions
App::error(function(\Exception $e)
{
	if (Config::get('app.debug') === true)
	{
		return null;
	}

	return View::make('error');
});

//404
App::error(function(NotFoundHttpException $e)
{
	if (Config::get('app.debug') == true)
	{
		return null;
	}

	return View::make('404');
});

