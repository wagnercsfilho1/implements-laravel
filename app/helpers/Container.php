<?php 

interface GreetableInterface{
	
	public function greet();

}

class HelloWorld implements GreetableInterface{

	public function greet()
	{
		return 'Hello, World!';
	}

}