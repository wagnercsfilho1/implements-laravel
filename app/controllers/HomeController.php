<?php

use Impl\Repo\Article\ArticleInterface;
use Impl\Service\Form\Article\ArticleForm;
use Impl\Repo\Status\StatusInterface;

class HomeController extends BaseController {

	protected $article;
	protected $articleform;
	protected $status;

	public function __construct(ArticleInterface $article, ArticleForm $articleform, StatusInterface $status){

		$this->article = $article;
		$this->articleform = $articleform;
		$this->status = $status;

	}


	/**
     * Paginated articles
     * GET /
    */
	public function index()
	{
		
		$page = Input::get('page',1);

		$articles = $this->article->byPage($page);

		$paginated = Paginator::make($articles->items, $articles->totalItems, $articles->limit);

		return View::make('home.index')->with('articles', $paginated);
	}

 	 /**
     * Single article
     * GET /{slug}
     */
	public function show($slug)
	{
		$article = $this->article->bySlug($slug);

		if( ! $article )
        {
            App::abort(404);
        }

		return View::make('home.show')->with('article', $article);
	}

	public function create()
	{
		$statuses = $this->status->all();

		return View::make('home.create')->with('statuses', $statuses);
	}

	public function save()
	{
		$input = array_merge(Input::all(), array('user_id' => 1));

		if( $this->articleform->save( $input ) )
        {
            // Success!
            //return Redirect::to('/admin/article')
            //        ->with('status', 'success');
            return Response::json(array('save'=>true));
        } else {
           // return Redirect::to('/admin/article/create')
            //        ->withInput()
           //         ->withErrors( $this->articleform->errors() )
            //        ->with('status', 'error');
            return Response::json($this->articleform->errors());
        }
		
	}

}
