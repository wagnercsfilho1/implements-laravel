<?php

use Impl\Repo\User\UserInterface;

class ProfileController extends BaseController {

	protected $user;

	public function __construct(UserInterface $user)
	{
		$this->user = $user;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($username)
	{
		$user = $this->user->byUsername($username);

		$articles = $user->articles()->paginate(15);

		//$articles = Paginator::make($article, $totalItems, 5);

		return View::make('profile.show')->with('user', $user)->with('articles', $articles);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
