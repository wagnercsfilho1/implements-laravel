<?php

use Impl\Repo\Article\ArticleInterface;
use Impl\Service\Form\Article\ArticleForm;

class ContentController extends \BaseController {

	protected $article;
	protected $articleform;

	// Class Dependency: Subclass of ArticleInterface
	public function __construct(ArticleInterface $article, ArticleForm $articleform)
	{
		$this->article = $article;
		$this->articleform = $articleform;
	}

	public function home()
	{
		$page = Input::get('page', 1);
		$perPage = 10;

		$pagiData = $this->article->byPage($page, $perPage);

		$articles = Paginator::make(
			$pagiData->items,
			$pagiData->totalItems,
			$perPage
		);

		//return View::make('home')->with('articles', $articles);
		return Response::Json($pagiData);
	}

	public function store()
	{
		if ($this->articleform->save( Input::all() ))
		{
			// Success!
		}else{
			return View::make('admin.article_create')
						->withInput( Input::all() )
						->withErrors( $this->articleform->errors() )
						->with('status', 'error');
		}
	}

	public function update()
	{
		// FORM PROCESSING
		if ($this->articleform->update(Input::all()))
		{
			// Success!
		}
	}


}
